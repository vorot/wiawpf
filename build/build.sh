#!/bin/sh

# Save the current directory - just in case
CURDIR=`pwd`
echo "Release build launched from $CURDIR..."

# The root path is relative to the position of this script
MYROOT=$(dirname $0)/..

# Include local variables
LOCAL_VARS=$WIAWPF_CONFIG;[ -f $LOCAL_VARS ] && . $LOCAL_VARS

# Provide default values for commands (if not specified in local.sh)
: ${CMD_BUILD:="$WINDIR/Microsoft.NET/Framework64/v4.0.30319/MSBuild.exe"}
: ${PAR_BUILD:="/verbosity:quiet /property:Configuration=""Release"""}
: ${CMD_DEPLOY:="mv"}
: ${PAR_DEPLOY:="-f"}
: ${DST_DEPLOY:="$MYROOT"}

# Build projects
"$CMD_BUILD" $PAR_BUILD "$MYROOT/wiawpf/wiawpf/wiawpf.csproj"
"$CMD_BUILD" $PAR_BUILD "$MYROOT/wiawpf/setup/setup.wixproj"

# Deploy artifacts
"$CMD_DEPLOY" $PAR_DEPLOY "$MYROOT/wiawpf/setup/bin/Release/setup.msi" "$DST_DEPLOY"

# Restore the current directory
echo "Build finished in `pwd`."
cd $CURDIR
