#!/bin/sh

# Save the current directory - just in case
CURDIR=`pwd`
echo "Deploy launched from $CURDIR..."

# The root path is relative to the position of this script
MYROOT=$(dirname $0)/..

# test
ls -l "$MYROOT/wiawpf"
ls -l "$MYROOT/wiawpf/setup"
ls -l "$MYROOT/wiawpf/setup/bin/Release"

# Restore the current directory
echo "Deploy finished in `pwd`."
cd $CURDIR
