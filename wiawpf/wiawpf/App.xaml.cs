﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace wiawpf
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            VM.ViewModel.SetCulture();
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            wiawpf.Properties.Settings.Default.Save();
        }
    }
}
