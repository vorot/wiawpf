﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;

namespace wiawpf.VM
{
    static class Extensions
    {
        public static void Sort<T>(this ObservableCollection<T> collection, Comparison<T> comparison)
        {
            List<T> sorted = collection.ToList();
            sorted.Sort(comparison);

            collection.Clear();
            foreach (T sortedItem in sorted)
            {
                collection.Add(sortedItem);
            }
        }
    }
}
