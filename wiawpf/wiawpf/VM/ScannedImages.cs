﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace wiawpf.VM
{
    public class ScannedImages : ObservableObject
    {

        public ScannedImages()
        {
            RootDir = Path.Combine(Environment.ExpandEnvironmentVariables(Properties.Settings.Default.TemporaryDirectory), "Scanner");
        }

        string m_RootDir;
        public string RootDir
        {
            get { return m_RootDir; }
            set
            {
                if (string.IsNullOrEmpty(value)) { return; }
                string newRootDir = Path.GetFullPath(value);
                if (!string.IsNullOrEmpty(m_RootDir) && Path.GetFullPath(m_RootDir).Equals(newRootDir)) { return; }

                DirectoryInfo newDir = Directory.CreateDirectory(newRootDir);

                if (newDir.Exists)
                {
                    m_RootDir = newRootDir;
                }

                reload();
            }
        }

        string m_DefaultExtension = "jpg";
        public string DefaultExtension { get { return m_DefaultExtension; } set { m_DefaultExtension = value; } }

        string m_Summary;
        public string Summary { get { return m_Summary; } set { SetAndNotify(value, () => this.Summary, (v) => this.m_Summary = v); } }

        ObservableCollection<ScannedImage> m_Images = new ObservableCollection<ScannedImage>();
        public ObservableCollection<ScannedImage> Images { get { return m_Images; } }

        void addNewImage(string filename)
        {
            App.Current.Dispatcher.Invoke((Action)(delegate { m_Images.Add(ScannedImage.ReadFromFile(filename)); }));
        }

        void reload()
        {
            Dictionary<string, ScannedImage> newImages = new Dictionary<string, ScannedImage>(StringComparer.InvariantCultureIgnoreCase);

            // generate new list of images
            foreach (string file in Directory.GetFiles(RootDir))
            {
                newImages.Add(file, ScannedImage.ReadFromFile(Path.GetFullPath(file)));
            }

            // Find indices of removed images and filter out unchanged images
            List<int> toRemove = new List<int>();
            for (int i = 0; i < m_Images.Count; i++)
            {
                if (newImages.ContainsKey(m_Images[i].FileName))
                {
                    newImages.Remove(m_Images[i].FileName);
                }
                else
                {
                    toRemove.Add(i);
                }
            }

            // Remove removed images
            for (int i = toRemove.Count - 1; i >= 0; i--)
            {
                m_Images.RemoveAt(toRemove[i]);
            }

            // Add new images
            foreach (ScannedImage img in newImages.Values)
            {
                m_Images.Add(img);
            }

            // Sort by Sequential number
            m_Images.Sort((x, y) => x.SequentialNumber.CompareTo(y.SequentialNumber));

            updateSummary();
        }

        class NativeMethods
        {
            [DllImport("Shlwapi.dll", CharSet = CharSet.Unicode)]
            public static extern long StrFormatByteSize(long fileSize, System.Text.StringBuilder buffer, int bufferSize);
        }

        public static string StrFormatByteSize(long filesize)
        {
            StringBuilder sb = new StringBuilder(11);
            NativeMethods.StrFormatByteSize(filesize, sb, sb.Capacity);
            return sb.ToString();
        }

        void updateSummary()
        {
            if (m_Images.Count == 0)
            {
                Summary = Properties.Resources.ScannedImages_NoImages;
                return;
            }

            long totalSize = m_Images.Sum(x => x.SizeBytes);
            int minHeight = m_Images.Min(x => x.PixelHeight);
            int maxHeight = m_Images.Max(x => x.PixelHeight);
            int minWidth = m_Images.Min(x => x.PixelWidth);
            int maxWidth = m_Images.Max(x => x.PixelWidth);
            double minDpiX = m_Images.Min(x => x.DpiX);
            double maxDpiX = m_Images.Max(x => x.DpiX);
            double minDpiY = m_Images.Min(x => x.DpiY);
            double maxDpiY = m_Images.Max(x => x.DpiY);

            string width = minWidth == maxWidth ? maxWidth.ToString() : string.Format("{0}..{1}", minWidth, maxWidth);
            string height = minHeight == maxHeight ? maxHeight.ToString() : string.Format("{0}..{1}", minHeight, maxHeight);

            string dpiX = minDpiX == maxDpiX ? maxDpiX.ToString() : string.Format("{0}..{1}", minDpiX, maxDpiX);
            string dpiY = minDpiY == maxDpiY ? maxDpiY.ToString() : string.Format("{0}..{1}", minDpiY, maxDpiY);

            Summary = string.Format("{0} file(s), {1}, {2}x{3}px, {4}x{5}dpi", new object[] { m_Images.Count, StrFormatByteSize(totalSize), width, height, dpiX, dpiY });
        }

        string GetNextFileName()
        {
            int nextId = m_Images.Count == 0 ? 1 : m_Images.Max(i => i.ScanId) + 1;
            return string.Format("{0}/{1}.{2}", RootDir, nextId, DefaultExtension);
        }

        public bool ScanNewFile()
        {
            Summary = Properties.Resources.ScannedImage_BusyScanning;
            WIA.ImageFile scanned = Scanner.Scan();

            if (scanned != null)
            {
                string newFile = GetNextFileName();
                scanned.SaveFile(newFile);
                addNewImage(newFile);

                updateSummary();
                return true;
            }

            updateSummary();
            return false;
        }

        public bool AddFile(string filename)
        {
            if (filename.StartsWith(RootDir))
            {
                return false;
            }

            string newFile = GetNextFileName();
            File.Copy(filename, newFile);
            m_Images.Add(ScannedImage.ReadFromFile(newFile));

            setSequentialNumbers();

            updateSummary();

            return true;
        }

        public void RemoveImages(IList<ScannedImage> images)
        {
            foreach (ScannedImage image in images)
            {
                if (image.FileName.StartsWith(RootDir))
                {
                    File.Delete(image.FileName);
                }
            }

            setSequentialNumbers();

            reload();
        }

        public void RotateImages(IList<ScannedImage> images, int degrees)
        {
            foreach (ScannedImage image in images)
            {
                if (image.FileName.StartsWith(RootDir))
                {
                    image.Rotation = (image.Rotation + degrees) % 360;
                }
            }
        }

        void setSequentialNumbers()
        {
            for (int i = 0; i < m_Images.Count; i++)
            {
                m_Images[i].SequentialNumber = i + 1;
            }
        }

        public void MoveLeft(IList<ScannedImage> images)
        {
            if (m_Images.Count < 2)
            {
                return;
            }

            Dictionary<int, ScannedImage> byId = new Dictionary<int, ScannedImage>();
            foreach (ScannedImage image in images)
            {
                byId.Add(image.ScanId, image);
            }

            for (int i = 1; i < m_Images.Count; i++)
            {
                ScannedImage curImage = m_Images[i];
                if (byId.ContainsKey(curImage.ScanId))
                {
                    m_Images.Move(i, i - 1);
                }
            }

            // Sort by Sequential number
            setSequentialNumbers();
        }

        public void MoveRight(IList<ScannedImage> images)
        {
            if (m_Images.Count < 2)
            {
                return;
            }

            Dictionary<int, ScannedImage> byId = new Dictionary<int, ScannedImage>();
            foreach (ScannedImage image in images)
            {
                byId.Add(image.ScanId, image);
            }

            for (int i = m_Images.Count - 2; i >= 0; i--)
            {
                ScannedImage curImage = m_Images[i];
                if (byId.ContainsKey(curImage.ScanId))
                {
                    m_Images.Move(i, i + 1);
                }
            }

            setSequentialNumbers();
        }

        bool findFirstNonExistingFileNumber(string template, ref int start, ref string fileName)
        {
            fileName = string.Format(template, start);

            // Verify if the template is changing when the number is changing
            if (fileName.Equals(string.Format(template, start + 1)))
            {
                return !File.Exists(fileName);
            }

            while (File.Exists(fileName))
            {
                fileName = string.Format(template, ++start);
            }

            return !File.Exists(fileName);
        }

        public static void SelectFileInExplorer(string fileName)
        {
            string args = string.Format("/e,/select,\"{0}\"", Path.GetFullPath(fileName));

            ProcessStartInfo info = new ProcessStartInfo();
            info.FileName = "explorer";
            info.Arguments = args;
            Process.Start(info);
        }

        public void SavePdf(IList<ScannedImage> images)
        {
            string outDir = Environment.ExpandEnvironmentVariables(Properties.Settings.Default.OutputDirectory);
            Directory.CreateDirectory(outDir);

            string template = Path.Combine(outDir, @"{0,8:00000000}.pdf");
            string fileName = string.Empty;
            int fileId = 1;

            if (findFirstNonExistingFileNumber(template, ref fileId, ref fileName))
            {
                org.pdfclown.files.File file = new org.pdfclown.files.File();
                org.pdfclown.documents.Document doc = file.Document;

                foreach (Image image in getProcessedImages(images))
                {
                    float imageWidthInBp = 72 * image.Width / image.HorizontalResolution;
                    float imageHeightInBp = 72 * image.Height / image.VerticalResolution;

                    SizeF pageSize = org.pdfclown.documents.PageFormat.GetSize(
                        org.pdfclown.documents.PageFormat.SizeEnum.A4,
                        imageHeightInBp >= imageWidthInBp ? org.pdfclown.documents.PageFormat.OrientationEnum.Portrait : org.pdfclown.documents.PageFormat.OrientationEnum.Landscape
                        );

                    if (pageSize.Width < imageWidthInBp)
                    {
                        pageSize.Width = imageWidthInBp;
                    }

                    if (pageSize.Height < imageHeightInBp)
                    {
                        pageSize.Height = imageHeightInBp;
                    }

                    org.pdfclown.documents.Page page = new org.pdfclown.documents.Page(doc, pageSize);

                    doc.Pages.Add(page);
                    org.pdfclown.documents.contents.composition.PrimitiveComposer composer = new org.pdfclown.documents.contents.composition.PrimitiveComposer(page);

                    using (MemoryStream ms = new MemoryStream())
                    {
                        image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                        ms.Position = 0;
                        org.pdfclown.documents.contents.entities.Image img = org.pdfclown.documents.contents.entities.Image.Get(ms);

                        double x = (pageSize.Width - imageWidthInBp) / 2;
                        double y = (pageSize.Height - imageHeightInBp) / 2;

                        composer.ApplyMatrix(imageWidthInBp, 0, 0, imageHeightInBp, x, y);
                        img.ToInlineObject(composer);
                    }

                    composer.Flush();
                }

                org.pdfclown.documents.interaction.viewer.ViewerPreferences view = new org.pdfclown.documents.interaction.viewer.ViewerPreferences(doc); // Instantiates viewer preferences inside the document context.
                doc.ViewerPreferences = view; // Assigns the viewer preferences object to the viewer preferences function.
                view.DisplayDocTitle = true;

                // Document metadata.
                org.pdfclown.documents.interchange.metadata.Information info = doc.Information;
                info.Clear();
                info.Author = string.Format(Properties.Resources.PDF_Author, DateTime.Now, Environment.UserName, Environment.MachineName);
                info.CreationDate = DateTime.Now;
                info.Creator = Properties.Resources.MainWindow_Title;
                info.Title = string.Format(Properties.Resources.PDF_Title, DateTime.Now, Environment.UserName, Environment.MachineName);
                info.Subject = string.Format(Properties.Resources.PDF_Subject, DateTime.Now, Environment.UserName, Environment.MachineName);
                info.Keywords = string.Format(Properties.Resources.PDF_Keywords, DateTime.Now, Environment.UserName, Environment.MachineName);

                file.Save(fileName, org.pdfclown.files.SerializationModeEnum.Standard);
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                SelectFileInExplorer(fileName);
            }
        }

        public void SaveJpg(IList<ScannedImage> images)
        {
            string outDir = Environment.ExpandEnvironmentVariables(Properties.Settings.Default.OutputDirectory);
            Directory.CreateDirectory(outDir);

            string template = Path.Combine(outDir, @"{0,8:00000000}.jpg");
            int fileId = 1;
            string fileName = string.Empty;
            string lastFileName = string.Empty;

            if (findFirstNonExistingFileNumber(template, ref fileId, ref fileName))
            {
                foreach (Image image in getProcessedImages(images))
                {
                    image.Save(fileName);

                    lastFileName = fileName;
                    fileName = string.Format(template, ++fileId);
                }
            }

            if (!string.IsNullOrEmpty(lastFileName))
            {
                SelectFileInExplorer(lastFileName);
            }

        }

        public void SavePng(IList<ScannedImage> images)
        {
            string outDir = Environment.ExpandEnvironmentVariables(Properties.Settings.Default.OutputDirectory);
            Directory.CreateDirectory(outDir);

            string template = Path.Combine(outDir, @"{0,8:00000000}.png");
            int fileId = 1;
            string fileName = string.Empty;
            string lastFileName = string.Empty;

            if (findFirstNonExistingFileNumber(template, ref fileId, ref fileName))
            {
                foreach (Image image in getProcessedImages(images))
                {
                    image.Save(fileName);

                    lastFileName = fileName;
                    fileName = string.Format(template, ++fileId);
                }
            }


            if (!string.IsNullOrEmpty(lastFileName))
            {
                SelectFileInExplorer(lastFileName);
            }
        }

        Image joinImages(IList<Image> images)
        {
            Debug.Assert(images.Count > 0);

            if (images.Count == 1)
            {
                return images[0];
            }
            else
            {
                float maxHR = images.Max(x => x.HorizontalResolution);
                float maxVR = images.Max(x => x.VerticalResolution);

                // images are joined in rows; rows >= cols

                int num_rows = (int)Math.Sqrt(images.Count);
                if (num_rows * num_rows < images.Count)
                {
                    num_rows += 1;
                }
                int num_cols = images.Count % num_rows == 0 ? images.Count / num_rows : (images.Count / num_rows) + 1;

                int totalWidth = 0;
                int totalHeight = 0;

                for (int i = 0; i < images.Count; i += num_cols)
                {
                    int rowWidth = (int)images.Skip(i).Take(num_cols).Sum(x => maxHR * x.Width / x.HorizontalResolution);
                    if (num_cols > 1) { rowWidth += 10 * (num_cols - 1); }
                    int rowHeight = (int)images.Skip(i).Take(num_cols).Max(x => maxVR * x.Height / x.VerticalResolution);
                    totalHeight += rowHeight;
                    if (rowWidth > totalWidth) { totalWidth = rowWidth; }
                }
                if (num_rows > 1) { totalHeight += 10 * (num_rows - 1); }

                System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(totalWidth, totalHeight);
                bitmap.SetResolution(maxHR, maxVR);

                //get a graphics object from the image so we can draw on it
                using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bitmap))
                {
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                    g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;

                    //set background color
                    g.Clear(System.Drawing.Color.White);

                    //go through each image and draw it on the final image
                    int voffset = 0;
                    int hoffset = 0;
                    int nextRow = num_cols;
                    int maxHeight = 0;

                    for (int i = 0; i < images.Count; i++)
                    {
                        if (i == nextRow)
                        {
                            hoffset = 0;
                            voffset += maxHeight;
                            voffset += 10;
                            nextRow += num_cols;
                        }

                        int scaledWidth = (int)(bitmap.HorizontalResolution * images[i].Width / images[i].HorizontalResolution);
                        int scaledHeight = (int)(bitmap.VerticalResolution * images[i].Height / images[i].VerticalResolution);

                        g.DrawImage(images[i],
                            new Rectangle(hoffset, voffset, scaledWidth, scaledHeight),
                            new Rectangle(0, 0, images[i].Width, images[i].Height),
                            GraphicsUnit.Pixel);

                        hoffset += scaledWidth;
                        hoffset += 10;
                        if (scaledHeight > maxHeight) { maxHeight = scaledHeight; }
                    }
                }

                MemoryStream ms = new MemoryStream();
                bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                ms.Position = 0;

                return Image.FromStream(ms);
            }
        }

        IEnumerable<Image> getProcessedImages(IList<ScannedImage> images)
        {
            List<Image> joined = new List<Image>();

            foreach (ScannedImage image in images)
            {
                if (image.Joined)
                {
                    // will be returned later
                }
                else
                {
                    // return all joined images
                    if (joined.Count > 0)
                    {
                        yield return joinImages(joined);
                        joined.Clear();
                    }
                }
                joined.Add(image.GetProcessedImage());
            }

            // last image if any
            if (joined.Count > 0)
            {
                yield return joinImages(joined);
                joined.Clear();
            }
        }
    }
}
