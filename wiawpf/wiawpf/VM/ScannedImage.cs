﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using System.Drawing;

namespace wiawpf.VM
{
    public class ScannedImage : ObservableObject
    {
        ScannedImage(string fileName)
        {
            m_FileName = Path.GetFullPath(fileName);
        }

        public static ScannedImage ReadFromFile(string fileName)
        {
            ScannedImage image = new ScannedImage(fileName);

            try
            {
                using (FileStream stream = File.OpenRead(fileName))
                {
                    BitmapFrame frame = BitmapFrame.Create(stream, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);

                    image.SizeBytes = stream.Length;
                    image.PixelWidth = frame.PixelWidth;
                    image.PixelHeight = frame.PixelHeight;
                    image.DpiX = frame.DpiX;
                    image.DpiY = frame.DpiY;
                }

                image.PreviewBitmap = ReadPreviewBitmap(fileName, 128, 128, image.PixelWidth, image.PixelHeight);
            }
            catch (Exception)
            {
                image.SizeBytes = 0;
            }

            image.updateProperties();

            return image;
        }

        // All the information is encoded into the filename
        // Examples:
        // Scanned image #1, rotation 180
        // c:/temp/scanned/0deeab20/001_r-180.jpg
        // Scanned image #2, moved to position 3
        // c:/temp/scanned/0deeab20/002_p-003.jpg
        // Scanned image #3, moved to position 2 and rotated by 90
        // c:/temp/scanned/0deeab20/003_p-002_r-90.jpg

        public string Location { get { return Path.GetDirectoryName(m_FileName); } }
        Dictionary<string, string> m_Properties = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
        int m_ScanId;
        public int ScanId { get { return m_ScanId; } }
        void updateProperties()
        {
            string[] parts = Path.GetFileNameWithoutExtension(m_FileName).Split(new char[] { '_' });
            m_ScanId = 0;
            int.TryParse(parts[0], out m_ScanId);
            m_Properties.Clear();
            for (int i = 1; i < parts.Length; i++)
            {
                string[] property = parts[i].Split(new char[] { '-' });
                if (property.Length == 2)
                {
                    SetProperty(property[0], property[1], string.Empty);
                }
            }
        }

        void updateFilename()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(ScanId.ToString());

            foreach (KeyValuePair<string, string> kv in m_Properties)
            {
                sb.AppendFormat("_{0}-{1}", kv.Key, kv.Value);
            }

            sb.Append(Path.GetExtension(FileName));

            FileName = Path.Combine(Path.GetDirectoryName(FileName), sb.ToString());
        }

        string GetProperty(string name, string def)
        {
            if (m_Properties.ContainsKey(name))
            {
                return m_Properties[name];
            }

            return def;
        }
        int GetProperty(string name, int def)
        {
            string prop = GetProperty(name, def.ToString());
            int result = def;

            int.TryParse(prop, out result);

            return result;
        }
        void RemoveProperty(string name)
        {
            if (string.IsNullOrEmpty(name)) { return; }

            if (m_Properties.ContainsKey(name))
            {
                m_Properties.Remove(name);
            }
        }
        void SetProperty(string name, string value, string def)
        {
            if (string.IsNullOrEmpty(name)) { return; }
            if (string.IsNullOrEmpty(value)) { RemoveProperty(name); return; }
            if (value.Equals(def)) { RemoveProperty(name); return; }

            if (m_Properties.ContainsKey(name))
            {
                m_Properties[name] = value;
            }
            else
            {
                m_Properties.Add(name, value);
            }
        }

        public int SequentialNumber
        {
            get { return GetProperty("#", ScanId); }
            set { SetAndNotify(value, () => this.SequentialNumber, (v) => this.SetProperty("#", v.ToString(), ScanId.ToString())); updateFilename(); }
        }
        public int Rotation
        {
            get { return GetProperty("r", 0); }
            set { SetAndNotify(value, () => this.Rotation, (v) => this.SetProperty("r", v.ToString(), "0")); updateFilename(); OnPropertyChanged(() => this.ProcessedImage); }
        }
        public bool Joined
        {
            get { return GetProperty("j", 0) == 0 ? false : true; }
            set { SetAndNotify(value, () => this.Joined, (v) => this.SetProperty("j", v ? "1" : "0", "0")); updateFilename(); OnPropertyChanged(() => this.PageNumber); }
        }

        public static BitmapImage ReadPreviewBitmap(string source, int previewWidth, int previewHeight, int sourceWidth, int sourceHeight)
        {
            BitmapImage bitmap = new BitmapImage();

            double sourceRatio = sourceHeight == 0 ? 0.0 : (double)sourceWidth / sourceHeight;
            double previewRatio = previewHeight == 0 ? 0.0 : (double)previewWidth / previewHeight;

            using (FileStream stream = File.OpenRead(source))
            {
                bitmap.BeginInit();

                bitmap.CacheOption = BitmapCacheOption.OnLoad;
                bitmap.StreamSource = stream;
                if (sourceRatio > previewRatio)
                {
                    bitmap.DecodePixelWidth = previewWidth;
                }
                else
                {
                    bitmap.DecodePixelHeight = previewHeight;
                }

                bitmap.EndInit();
            }

            return bitmap;
        }

        BitmapImage m_PreviewBitmap = null;
        public BitmapImage PreviewBitmap { get { return m_PreviewBitmap; } set { SetAndNotify(value, () => this.PreviewBitmap, (v) => this.m_PreviewBitmap = v); } }

        string m_FileName;
        public string FileName
        {
            get { return m_FileName; }
            set
            {
                if (Path.GetFullPath(m_FileName).Equals(Path.GetFullPath(value))) { return; }
                try
                {
                    System.IO.File.Move(m_FileName, value);
                    SetAndNotify(value, () => this.FileName, (v) => this.m_FileName = v);
                    updateProperties();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Format("Cannot move {0} to {1}: {2}", m_FileName, value, ex.Message));
                }
            }
        }

        long m_SizeBytes;
        public long SizeBytes { get { return m_SizeBytes; } set { SetAndNotify(value, () => this.SizeBytes, (v) => this.m_SizeBytes = v); } }

        double m_DpiX;
        public double DpiX { get { return m_DpiX; } set { SetAndNotify(value, () => this.DpiX, (v) => this.m_DpiX = v); } }

        double m_DpiY;
        public double DpiY { get { return m_DpiY; } set { SetAndNotify(value, () => this.DpiY, (v) => this.m_DpiY = v); } }

        int m_PixelWidth;
        public int PixelWidth { get { return m_PixelWidth; } set { SetAndNotify(value, () => this.PixelWidth, (v) => this.m_PixelWidth = v); } }

        int m_PixelHeight;
        public int PixelHeight { get { return m_PixelHeight; } set { SetAndNotify(value, () => this.PixelHeight, (v) => this.m_PixelHeight = v); } }

        public string Info { get { return string.Format("{0}, {1}x{2}px, {3}x{4}dpi", new object[] { ScannedImages.StrFormatByteSize(m_SizeBytes), PixelWidth, PixelHeight, DpiX, DpiY }); } }

        public Image GetProcessedImage()
        {
            Image image = Image.FromFile(FileName);

            switch (Rotation)
            {
                case 0:
                    break;
                case 90:
                    image.RotateFlip(RotateFlipType.Rotate90FlipNone);
                    break;
                case 180:
                    image.RotateFlip(RotateFlipType.Rotate180FlipNone);
                    break;
                case 270:
                    image.RotateFlip(RotateFlipType.Rotate270FlipNone);
                    break;
                default:
                    throw new NotImplementedException(string.Format("Rotation angle {0} is not supported", Rotation));
            }

            return image;
        }

        public BitmapImage ProcessedImage
        {
            get
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    using (Bitmap bmp = (Bitmap)GetProcessedImage())
                    {
                        bmp.SetResolution(72, 72);
                        bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                        ms.Position = 0;

                        BitmapImage result = new BitmapImage();

                        result.BeginInit();
                        result.CacheOption = BitmapCacheOption.OnLoad;
                        result.UriSource = null;
                        result.StreamSource = ms;
                        result.EndInit();

                        return result;
                    }
                }
            }
        }

        public string PageNumber
        {
            get { return string.Format("{0}{1}", SequentialNumber.ToString(), Joined ? " (joined)" : string.Empty); }
        }

        public override string ToString()
        {
            return string.Format("#{0}:{1}", SequentialNumber, Path.GetFileName(FileName));
        }
    }
}
