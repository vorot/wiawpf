﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;

namespace wiawpf.VM
{
    public class ObservableObject : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void SetAndNotify<T>(T value, Expression<Func<T>> property, Action<T> setProperty)
        {
            if (!object.Equals(property, value))
            {
                setProperty(value);
                this.OnPropertyChanged(property);
            }
        }

        protected virtual void OnPropertyChanged<T>(Expression<Func<T>> changedProperty)
        {
            if (PropertyChanged != null)
            {
                string name = ((MemberExpression)changedProperty.Body).Member.Name;
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
