﻿using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Threading;
using System.Windows;
using System.Windows.Data;
using WinForms = System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Markup.Primitives;
using wiawpf.Properties;

namespace wiawpf.VM
{

    public class ViewModel : ObservableObject
    {
        ScannedImages m_Images = new ScannedImages();
        public ScannedImages ScannedImages { get { return m_Images; } }

        AsyncRelayCommand m_scanCommand = null;
        public ICommand ScanCommand { get { return AsyncRelayCommand.CreateOnce(ref m_scanCommand, param => this.scan(), param => true); } }
        void scan()
        {
            try
            {
                m_Images.ScanNewFile();
            }
            catch (Scanner.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        RelayCommand m_LoadImageCommand = null;
        public ICommand LoadImageCommand { get { return RelayCommand.CreateOnce(ref m_LoadImageCommand, param => this.loadImage(), param => true); } }
        void loadImage()
        {
            try
            {
                OpenFileDialog dialog = new OpenFileDialog();

                dialog.DefaultExt = ".jpg";
                dialog.Filter = "Images (.jpg,.jpeg,.png)|*.jpg;*.jpeg;*.png";
                dialog.Multiselect = true;

                if (dialog.ShowDialog() == true)
                {
                    foreach (string s in dialog.FileNames)
                    {
                        m_Images.AddFile(s);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        IList<ScannedImage> getImages(object param)
        {
            List<ScannedImage> images = new List<ScannedImage>();
            IList items = param as IList;

            if (items != null)
            {
                foreach (object o in items)
                {
                    ScannedImage image = o as ScannedImage;
                    if (image != null)
                    {
                        images.Add(image);
                    }
                }
            }

            return images;
        }

        bool hasItems(object param)
        {
            IList items = param as IList;

            if (items == null)
            {
                Debug.WriteLine(param == null ? "<null>" : param.GetType().ToString());
            }

            return items == null ? false : items.Count > 0;
        }

        RelayCommand m_rotate90Command = null;
        public ICommand Rotate90Command { get { return RelayCommand.CreateOnce(ref m_rotate90Command, param => this.Rotate90(param), param => this.hasItems(param)); } }
        void Rotate90(object param)
        {
            try
            {
                m_Images.RotateImages(getImages(param), 90);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        RelayCommand m_rotate180Command = null;
        public ICommand Rotate180Command { get { return RelayCommand.CreateOnce(ref m_rotate180Command, param => this.Rotate180(param), param => this.hasItems(param)); } }
        void Rotate180(object param)
        {
            try
            {
                m_Images.RotateImages(getImages(param), 180);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        RelayCommand m_rotate270Command = null;
        public ICommand Rotate270Command { get { return RelayCommand.CreateOnce(ref m_rotate270Command, param => this.Rotate270(param), param => this.hasItems(param)); } }
        void Rotate270(object param)
        {
            try
            {
                m_Images.RotateImages(getImages(param), 270);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        RelayCommand m_MoveLeftCommand = null;
        public ICommand MoveLeftCommand { get { return RelayCommand.CreateOnce(ref m_MoveLeftCommand, param => this.moveLeft(param), param => this.canMoveLeft(param)); } }
        bool canMoveLeft(object param)
        {
            IList<ScannedImage> selected = getImages(param);

            if (selected.Count > 0)
            {
                foreach (ScannedImage img in selected)
                {
                    if (ScannedImage.ReferenceEquals(img, ScannedImages.Images[0]))
                    {
                        return false;
                    }
                }

                return true;
            }

            return false;
        }
        void moveLeft(object param)
        {
            try
            {
                m_Images.MoveLeft(getImages(param));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        RelayCommand m_MoveRightCommand = null;
        public ICommand MoveRightCommand { get { return RelayCommand.CreateOnce(ref m_MoveRightCommand, param => this.moveRight(param), param => this.canMoveRight(param)); } }
        bool canMoveRight(object param)
        {
            IList<ScannedImage> selected = getImages(param);

            if (selected.Count > 0)
            {
                foreach (ScannedImage img in selected)
                {
                    if (ScannedImage.ReferenceEquals(img, ScannedImages.Images[ScannedImages.Images.Count - 1]))
                    {
                        return false;
                    }
                }

                return true;
            }

            return false;
        }
        void moveRight(object param)
        {
            try
            {
                m_Images.MoveRight(getImages(param));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        RelayCommand m_DeleteImagesCommand = null;
        public ICommand DeleteImagesCommand { get { return RelayCommand.CreateOnce(ref m_DeleteImagesCommand, param => this.deleteImages(param), param => this.hasItems(param)); } }
        void deleteImages(object param)
        {
            try
            {
                m_Images.RemoveImages(getImages(param));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        RelayCommand m_SavePdfCommand = null;
        public ICommand SavePdfCommand { get { return RelayCommand.CreateOnce(ref m_SavePdfCommand, param => this.savePdf(param), param => this.hasItems(param)); } }
        void savePdf(object param)
        {
            try
            {
                m_Images.SavePdf(getImages(param));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        RelayCommand m_SaveJpgCommand = null;
        public ICommand SaveJpgCommand { get { return RelayCommand.CreateOnce(ref m_SaveJpgCommand, param => this.saveJpg(param), param => this.hasItems(param)); } }
        void saveJpg(object param)
        {
            try
            {
                m_Images.SaveJpg(getImages(param));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        RelayCommand m_SavePngCommand = null;
        public ICommand SavePngCommand { get { return RelayCommand.CreateOnce(ref m_SavePngCommand, param => this.savePng(param), param => this.hasItems(param)); } }
        void savePng(object param)
        {
            try
            {
                m_Images.SavePng(getImages(param));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        RelayCommand m_ToggleJoinedCommand = null;
        public ICommand ToggleJoinedCommand { get { return RelayCommand.CreateOnce(ref m_ToggleJoinedCommand, param => this.toggleJoined(param), param => this.hasItems(param)); } }
        void toggleJoined(object param)
        {
            try
            {
                foreach (ScannedImage img in getImages(param))
                {
                    img.Joined = !img.Joined;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        RelayCommand m_ShowInFolderCommand = null;
        public ICommand ShowInFolderCommand { get { return RelayCommand.CreateOnce(ref m_ShowInFolderCommand, param => this.showInFolder(param), param => this.hasItems(param)); } }
        void showInFolder(object param)
        {
            try
            {
                foreach (ScannedImage img in getImages(param))
                {
                    ScannedImages.SelectFileInExplorer(img.FileName);
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Update all properties bound to properties of the given static class.
        /// Only update bindings like "{Binding Path=(namespace:staticClass.property)}".
        /// Bindings like "{Binding Source={x:Static namespace:staticClass.property}}" cannot be updated.
        /// </summary>
        /// <param name="obj">Object that must be updated, normally the main window</param>
        /// <param name="staticClass">The static class that is used as the binding source, normally Properties.Resources</param>
        /// <param name="recursive">true: update all child objects too</param>
        static void UpdateStaticBindings(DependencyObject obj, Type staticClass, bool recursive)
        {
            // Update bindings for all properties that are statically bound to
            // static properties of the given static class
            if (obj != null)
            {
                MarkupObject markupObject = MarkupWriter.GetMarkupObjectFor(obj);

                if (markupObject != null)
                {
                    foreach (MarkupProperty mp in markupObject.Properties)
                    {
                        if (mp.DependencyProperty != null)
                        {
                            BindingExpression be = BindingOperations.GetBindingExpression(obj, mp.DependencyProperty) as BindingExpression;

                            if (be != null)
                            {
                                // Only update bindings like "{Binding Path=(namespace:staticClass.property)}"
                                if (be.ParentBinding.Path.PathParameters.Count == 1)
                                {
                                    MemberInfo mi = be.ParentBinding.Path.PathParameters[0] as MemberInfo;

                                    if (mi != null && mi.DeclaringType.Equals(staticClass))
                                    {
                                        be.UpdateTarget();
                                        // System.Diagnostics.Debug.WriteLine(string.Format("Updating property {0} of object of type {1}",mp.DependencyProperty.Name,obj.GetType().Name));
                                    }
                                }
                            }
                        }
                    }
                }

                // Iterate children, if requested
                if (recursive)
                {
                    foreach (object child in LogicalTreeHelper.GetChildren(obj))
                    {
                        UpdateStaticBindings(child as DependencyObject, staticClass, true);
                    }
                }
            }
        }

        public static void SetCulture()
        {
            try
            {
                CultureInfo ci = new CultureInfo(Properties.Settings.Default.Culture);

                CultureInfo.DefaultThreadCurrentCulture = ci;
                CultureInfo.DefaultThreadCurrentUICulture = ci;
                Thread.CurrentThread.CurrentCulture = ci;
                Thread.CurrentThread.CurrentUICulture = ci;
                Properties.Resources.Culture = ci;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        public static IEnumerable<CultureInfo> GetAvailableCultures()
        {
            List<CultureInfo> result = new List<CultureInfo>();

            ResourceManager rm = new ResourceManager(typeof(Resources));

            CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.AllCultures);
            foreach (CultureInfo culture in cultures)
            {
                try
                {
                    if (culture.Equals(CultureInfo.InvariantCulture)) continue; //do not use "==", won't work

                    ResourceSet rs = rm.GetResourceSet(culture, true, false);
                    if (rs != null)
                        result.Add(culture);
                }
                catch (CultureNotFoundException)
                {
                    //NOP
                }
            }

            AddDebugCulture(ref result);

            return result;
        }

        [Conditional("DEBUG")]
        static void AddDebugCulture(ref List<CultureInfo> result)
        {
            result.Add(new CultureInfo("qps-ploc"));
        }

        public static ObservableCollection<string> Languages
        {
            get
            {
                var languages = new ObservableCollection<string>();
                var cultures = GetAvailableCultures();
                foreach (CultureInfo culture in cultures)
                    languages.Add(culture.Name);
                return languages;
            }
        }

        public string Culture
        {
            get { return Properties.Settings.Default.Culture; }
            set
            {
                SetAndNotify(value, () => Properties.Settings.Default.Culture, (v) => { Properties.Settings.Default.Culture = value; });
                SetCulture();
                UpdateStaticBindings(Application.Current.MainWindow, typeof(Properties.Resources), true);
            }
        }

        public string MainWindow_Title
        {
            get { return Properties.Resources.MainWindow_Title; }

        }

        RelayCommand m_SetLanguageCommand = null;
        public ICommand SetLanguageCommand { get { return RelayCommand.CreateOnce(ref m_SetLanguageCommand, param => this.setLanguage(param), param => true); } }
        void setLanguage(object param)
        {
            try
            {
                Culture = param.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        RelayCommand m_SelectOutputDirCommand = null;
        public ICommand SelectOutputDirCommand { get { return RelayCommand.CreateOnce(ref m_SelectOutputDirCommand, param => Properties.Settings.Default.OutputDirectory = this.selectDir(Properties.Settings.Default.OutputDirectory), param => true); } }
        RelayCommand m_SelectTempDirCommand = null;
        public ICommand SelectTempDirCommand { get { return RelayCommand.CreateOnce(ref m_SelectTempDirCommand, param => Properties.Settings.Default.TemporaryDirectory = this.selectDir(Properties.Settings.Default.TemporaryDirectory), param => true); } }

        string selectDir(string initial)
        {
            string result = initial;

            try
            {
                WinForms.FolderBrowserDialog dialog = new WinForms.FolderBrowserDialog();

                dialog.SelectedPath = Environment.ExpandEnvironmentVariables(initial);

                if (dialog.ShowDialog() == WinForms.DialogResult.OK)
                {
                    result = dialog.SelectedPath;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return result;
        }

        RelayCommand m_ResetSettingsCommand = null;
        public ICommand ResetSettingsCommand { get { return RelayCommand.CreateOnce(ref m_ResetSettingsCommand, param => Settings.Default.Reset(), param => true); } }

        VersionList m_VersionList = new VersionList();
        public VersionList About { get { return m_VersionList; } }
    }
}
