﻿using System.Collections.Generic;
using System.Reflection;

namespace wiawpf.VM
{
    public class VersionList
    {
        Assembly m_assembly = Assembly.GetEntryAssembly();

        public string Title { get { return m_assembly.GetCustomAttribute<AssemblyTitleAttribute>().Title; } }
        public string Description { get { return m_assembly.GetCustomAttribute<AssemblyDescriptionAttribute>().Description; } }
        public string Version { get { return m_assembly.GetName().Version.ToString(); } }
        public string Copyright { get { return m_assembly.GetCustomAttribute<AssemblyCopyrightAttribute>().Copyright; } }

        public class Entry
        {
            public Entry(string name, string url, string version, string author, string license, string licenseUrl)
            {
                m_Name = name;
                m_Url = url;
                m_Version = version;
                m_Author = author;
                m_License = license;
                m_LicenseUrl = licenseUrl;
            }

            string m_Name;
            public string Name { get { return m_Name; } }

            string m_Url;
            public string Url { get { return m_Url; } }

            string m_Version;
            public string Version { get { return m_Version; } }

            string m_Author;
            public string Author { get { return m_Author; } }

            public string m_License;
            public string License { get { return m_License; } }

            public string m_LicenseUrl;
            public string LicenseUrl { get { return m_LicenseUrl; } }
        }

        List<Entry> m_Entries = new List<Entry>();
        public IReadOnlyList<Entry> Entries { get { return m_Entries.AsReadOnly(); } }

        public VersionList()
        {
            m_Entries.Add(new Entry(this.Title, @"https://vorotilov.linkpc.net/wiawpf/", this.Version, this.Copyright, @"BSD 2-Clause", @"https://opensource.org/licenses/BSD-2-Clause"));
            m_Entries.Add(new Entry(@"PDFClown", @"http://pdfclown.org", @"0.1.2", @"Stefano Chizzolini", @"LGPL", @"http://www.gnu.org/copyleft/lgpl.html"));
            m_Entries.Add(new Entry(@"FatCow icon pack", @"http://www.fatcow.com/free-icons", @"3.9.2", @"FatCow", @"CC BY 3.0", @"https://creativecommons.org/licenses/by/3.0/"));
        }
    }
}
