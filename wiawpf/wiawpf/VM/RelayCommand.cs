﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;

namespace wiawpf.VM
{
    class RelayCommand : ICommand
    {
        #region Fields

        readonly Action<object> m_execute;
        readonly Predicate<object> m_canExecute;
        bool m_isExecuting;

        #endregion // Fields

        #region Constructors

        public RelayCommand(Action<object> execute)
            : this(execute, null)
        {
        }

        public static RelayCommand CreateOnce(ref RelayCommand member, Action<object> execute, Predicate<object> canExecute)
        {
            if (member == null)
            {
                member = new RelayCommand(execute, canExecute);
            }

            return member;
        }

        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");

            m_execute = execute;
            m_canExecute = canExecute;
        }
        #endregion // Constructors

        #region ICommand Members

        [DebuggerStepThrough]
        public bool CanExecute(object parameter)
        {
            return !m_isExecuting && (m_canExecute == null ? true : m_canExecute(parameter));
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            try
            {
                m_isExecuting = true;
                CommandManager.InvalidateRequerySuggested();
                m_execute(parameter);
            }
            finally
            {
                m_isExecuting = false;
            }
        }

        #endregion // ICommand Members
    }

    public interface IAsyncCommand : ICommand
    {
        Task ExecuteAsync(object parameter);
    }

    public class AsyncRelayCommand : IAsyncCommand
    {
        RelayCommand m_Command;
        Task m_task;

        public static AsyncRelayCommand CreateOnce(ref AsyncRelayCommand member, Action<object> execute, Predicate<object> canExecute)
        {
            if (member == null)
            {
                member = new AsyncRelayCommand(execute, canExecute);
            }

            return member;
        }

        public AsyncRelayCommand(Action<object> execute)
            : this(execute, _ => true)
        {
        }

        public AsyncRelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            m_Command = new RelayCommand(execute, canExecute);
        }

        public bool CanExecute(object parameter)
        {
            return m_Command.CanExecute(parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add { m_Command.CanExecuteChanged += value; }
            remove { m_Command.CanExecuteChanged -= value; }
        }

        public void Execute(object parameter)
        {
            m_task = ExecuteAsync(parameter);
        }

        public async Task ExecuteAsync(object parameter)
        {
            await Task.Run(() =>
            {
                m_Command.Execute(parameter);
                App.Current.Dispatcher.Invoke((Action)(delegate
                {
                    CommandManager.InvalidateRequerySuggested();
                }));
            });
        }
    }
}
