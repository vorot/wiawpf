﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using WIA;

namespace wiawpf.VM
{
    public class Scanner
    {
        [Serializable]
        public class Exception : ApplicationException
        {
            public Exception() : base() { }
            public Exception(string message) : base(message) { }
            public Exception(string message, System.Exception innerException) : base(message, innerException) { }
        }

        [Serializable]
        public class NotFoundException : Exception
        {
            public NotFoundException() : base(Properties.Resources.Scanner_NoScannerFound) { }
        }

        const string wiaFormatBMP = "{B96B3CAB-0728-11D3-9D7B-0000F81EF32E}";
        const string wiaFormatGIF = "{B96B3CB0-0728-11D3-9D7B-0000F81EF32E}";
        const string wiaFormatJPEG = "{B96B3CAE-0728-11D3-9D7B-0000F81EF32E}";
        const string wiaFormatPNG = "{B96B3CAF-0728-11D3-9D7B-0000F81EF32E}";
        const string wiaFormatTIFF = "{B96B3CB1-0728-11D3-9D7B-0000F81EF32E}";

        [DllImport("user32.dll")]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        static bool bringWIADialogToFront()
        {
            IntPtr dialog = WindowSearch.FindWindowByChildClass("WiaPreviewControlFrame");

            if (dialog != IntPtr.Zero)
            {
                return SetForegroundWindow(dialog);
            }

            return false;
        }

        static public ImageFile Scan()
        {
            // The WIA dialog can appear under the main window.
            // This asyncronous task will bring the WIA dialog to front as soon as it appears
            // If unsuccessful, it will simply minimize the main window
            DispatcherOperation operationBringToFront = App.Current.Dispatcher.BeginInvoke(
                DispatcherPriority.Background,
                (Action)(() =>
            {
                bool no_minimize = false;

                for (int i = 0; i < 15; i++)
                {
                    if (bringWIADialogToFront())
                    {
                        no_minimize = true;
                        break;
                    }

                    Thread.Sleep(100);
                }
                if (!no_minimize)
                {
                    App.Current.MainWindow.WindowState = System.Windows.WindowState.Minimized;
                }
            }));

            try
            {
                CommonDialog dialog = new CommonDialog();

                // Here we show the dialog without any possiblity to control its location
                return dialog.ShowAcquireImage(
                        WiaDeviceType.ScannerDeviceType,
                        WiaImageIntent.ColorIntent,
                        WiaImageBias.MaximizeQuality,
                        wiaFormatJPEG,
                        false,
                        true,
                        false);
            }
            catch (COMException ex)
            {
                if (ex.ErrorCode == -2145320939)
                {
                    throw new NotFoundException();
                }
                else
                {
                    throw new Exception("COM Exception", ex);
                }
            }
            finally
            {
                // Restore main window it it was minimized during scanning
                App.Current.Dispatcher.Invoke((Action)(() =>
                {
                    if (App.Current != null && App.Current.MainWindow != null)
                    {
                        App.Current.MainWindow.WindowState = System.Windows.WindowState.Normal;
                    }
                }));
            }
        }

        static public BitmapSource ScanBitmap()
        {
            ImageFile img = Scan();

            if (img != null)
            {
                string tmpFile = Path.GetTempFileName();
                File.Delete(tmpFile);
                img.SaveFile(tmpFile);

                BitmapFrame bmp = null;

                using (FileStream stream = File.OpenRead(tmpFile))
                {
                    bmp = BitmapFrame.Create(stream, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
                }

                File.Delete(tmpFile);

                return bmp;
            }

            return null;
        }
    }
}
