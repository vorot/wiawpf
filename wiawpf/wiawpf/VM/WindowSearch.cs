﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace wiawpf.VM
{
    class WindowSearch
    {
        [DllImport("user32.dll")]
        public static extern IntPtr GetParent(IntPtr hWnd);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool EnumChildWindows(IntPtr hWnd, EnumWindowsProc enumProc, IntPtr lParam);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);

        // Declaration of the callback function
        delegate bool EnumWindowsProc(IntPtr hWnd, IntPtr lParam);

        static bool IsFound(IntPtr hWnd, IntPtr lParam)
        {
            String name = Marshal.PtrToStringAuto(lParam);

            StringBuilder sb = new StringBuilder(256);
            int len = GetClassName(hWnd, sb, sb.Capacity);

            return String.Equals(sb.ToString(), name, StringComparison.InvariantCultureIgnoreCase);
        }

        static IntPtr findWindowRecursive(IntPtr hWnd, IntPtr controlClassString)
        {
            IntPtr found = IntPtr.Zero;

            EnumChildWindows(
                hWnd,
                delegate(IntPtr wnd, IntPtr param)
                {
                    if (IsFound(wnd, param))
                    {
                        found = wnd;
                        return false;
                    }
                    else
                    {
                        found = findWindowRecursive(wnd, param);
                        return found == IntPtr.Zero ? true : false;
                    }
                },
                controlClassString);

            return found;
        }

        /// <summary>
        /// Find the first window having the given window class
        /// </summary>
        /// <param name="windowClass">Window class to search for</param>
        /// <returns>HWND of the window having the given class</returns>
        public static IntPtr FindWindowByClass(string windowClass)
        {
            return findWindowRecursive(IntPtr.Zero, Marshal.StringToHGlobalAuto(windowClass));
        }

        /// <summary>
        /// Find the first top-level window with a child window that has the given window class
        /// </summary>
        /// <param name="childClass">Class of the child window</param>
        /// <returns>HWND of the top-level window</returns>
        public static IntPtr FindWindowByChildClass(string childClass)
        {
            IntPtr result = findWindowRecursive(IntPtr.Zero, Marshal.StringToHGlobalAuto(childClass));

            while (GetParent(result) != IntPtr.Zero)
            {
                result = GetParent(result);
            }

            return result;
        }
    }

}
